<?php if(!is_front_page()) : ?>
    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
        <?php if(is_active_sidebar('sidebar')): ?>
            <?php dynamic_sidebar('sidebar'); ?>
        <?php endif; ?>
    </div><!-- /.blog-sidebar -->
<?php endif; ?>
  </div><!-- /.row -->

  </div><!-- /.container -->

<div class="container">
<footer>
    <p>&copy; <?php echo Date('Y'); ?> - <?php bloginfo('name'); ?></p>
    <p class="pull-right"><a href="#">Back to top</a></p>
</footer>
</div>
<?php wp_footer(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
</body>
</html>