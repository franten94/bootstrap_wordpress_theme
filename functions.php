<?php 

// Register Custom Navigation Walker
require_once('wp-bootstrap-navwalker.php');

// Theme Support for WP BS Navwalker
function wpb_theme_setup(){
    add_theme_support('post-thumbnails');

    // Nav Menus
    register_nav_menus(array(
      'primary' => __('Primary Menu')
    ));

    // Post Formats
    add_theme_support('post-formats', array('aside', 'gallery'));
}
// Excerpt Length Control
function set_excerpt_length(){
  return 70;
}

// Widget Locations
function wpb_init_widgets($id){
  register_sidebar(array(
    'name'  => 'Sidebar',
    'id'    => 'sidebar',
    'before_widget' => '<div class="sidebar-module">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>'
  ));
}

//Hooks

// Main Function
add_action('after_setup_theme','wpb_theme_setup');
// Post Excerpt 
add_filter('excerpt_length', 'set_excerpt_length');
// Sidebar register
add_action('widgets_init', 'wpb_init_widgets');

?>
