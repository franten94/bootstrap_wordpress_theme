<div class="col-md-10 comments">
    <div class="row">
          <h3 class="col-md-offset-4 col-md-6">Comentarios</h3>
          <?php $args = array(
        	'walker'            => null,
        	'max_depth'         => '',
        	'style'             => 'ul',
        	'callback'          => null,
        	'end-callback'      => null,
        	'type'              => 'all',
        	'reply_text'        => 'Responder',
        	'page'              => '',
        	'per_page'          => '',
        	'avatar_size'       => 80,
        	'reverse_top_level' => null,
        	'reverse_children'  => '',
        	'format'            => 'html5', // or 'xhtml' if no 'HTML5' theme support
        	'short_ping'        => false,   // @since 3.6
                'echo'              => true     // boolean, default is true
        ); ?>
    </div>

<?php
wp_list_comments($args, $comments);

$comments_args = array(
        'label_submit'=>'Enviar',
        'title_reply'=>'Deja una Respuesta o Comentario',
        'comment_notes_after' => '',
        'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comentario', 'noun' ) . '</label><br /><textarea id="comment" name="comment" aria-required="true"></textarea></p>',
);

comment_form($comments_args);
?>
</div>
